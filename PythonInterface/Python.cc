
/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/

#include <pybind11/pybind11.h>
#include <pybind11/embed.h>


#include <PrimitivesGenerator.hh>
#include <QString>
#include <QChar>

#include <OpenFlipper/BasePlugin/PythonFunctions.hh>
#include <OpenFlipper/PythonInterpreter/PythonTypeConversions.hh>

#include <QCoreApplication>

namespace py = pybind11;



PYBIND11_EMBEDDED_MODULE(PrimitivesGenerator, m) {

  QObject* pluginPointer = getPluginPointer("PrimitivesGenerator");

  if (!pluginPointer) {
     std::cerr << "Error Getting plugin pointer for Plugin-PrimitivesGenerator" << std::endl;
     return;
   }

  PrimitivesGeneratorPlugin* plugin = qobject_cast<PrimitivesGeneratorPlugin*>(pluginPointer);

  if (!plugin) {
    std::cerr << "Error converting plugin pointer for Plugin-PrimitivesGenerator" << std::endl;
    return;
  }

  // Export our core. Make sure that the c++ worlds core object is not deleted if
  // the python side gets deleted!!
  py::class_< PrimitivesGeneratorPlugin,std::unique_ptr<PrimitivesGeneratorPlugin, py::nodelete> > generator(m, "PrimitivesGenerator");

  // On the c++ side we will just return the existing core instance
  // and prevent the system to recreate a new core as we need
  // to work on the existing one.
  generator.def(py::init([plugin]() { return plugin; }));


  generator.def("addTriangulatedCylinder", &PrimitivesGeneratorPlugin::addTriangulatedCylinder,
                                 QCoreApplication::translate("PythonDocPrimitivesGenerator","Generates a triangulated cylinder (ObjectId is returned)").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Position (Bottom center vertex position)").toLatin1().data()) = Vector(0.0,0.0,0.0),
                         py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Center axis of cylinder").toLatin1().data()) = Vector(0.0,0.0,1.0),
                         py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Radius").toLatin1().data()),
                         py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Height").toLatin1().data()),
                         py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Add Top vertex?").toLatin1().data()) = true,
                         py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Add bottom vertex?").toLatin1().data()) = true  );

  generator.def("addSphere", &PrimitivesGeneratorPlugin::addSphere,
                                 QCoreApplication::translate("PythonDocPrimitivesGenerator","Generates a triangulated sphere with all vertical lines connected to the poles (ObjectId is returned)").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Position (Sphere center)").toLatin1().data()) = Vector(0.0,0.0,0.0),
                         py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Radius").toLatin1().data()) = 1.0);

  generator.def("addSubdivisionSphere", &PrimitivesGeneratorPlugin::addSubdivisionSphere,
                                 QCoreApplication::translate("PythonDocPrimitivesGenerator","Generates a triangulated sphere by subdivision without poles. (ObjectId is returned)").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Position (Sphere center)").toLatin1().data()) = Vector(0.0,0.0,0.0),
                         py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Radius").toLatin1().data()) = 1.0);

  generator.def("addTetrahedron", &PrimitivesGeneratorPlugin::addTetrahedron,
                                 QCoreApplication::translate("PythonDocPrimitivesGenerator","Generates a tetrahedron (ObjectId is returned)").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Center position").toLatin1().data()) = Vector(0.0,0.0,0.0),
                         py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Length of each edge").toLatin1().data()) = 1.0);

  generator.def("addPyramid", &PrimitivesGeneratorPlugin::addPyramid,
                                 QCoreApplication::translate("PythonDocPrimitivesGenerator","Generates a pyramid (ObjectId is returned)").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Center position").toLatin1().data()) = Vector(0.0,0.0,0.0),
                         py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Length of each edge").toLatin1().data()) = 2.0);

  generator.def("addCube", &PrimitivesGeneratorPlugin::addCube,
                                 QCoreApplication::translate("PythonDocPrimitivesGenerator","Generates a poly mesh of cube (ObjectId is returned)").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Center position").toLatin1().data()) = Vector(0.0,0.0,0.0),
                         py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Length of each edge").toLatin1().data()) = 2.0);

  generator.def("addTriangulatedCube", &PrimitivesGeneratorPlugin::addTriangulatedCube,
                                  QCoreApplication::translate("PythonDocPrimitivesGenerator","Generates a triangular mesh of cube (ObjectId is returned)").toLatin1().data(),
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Center position").toLatin1().data()) = Vector(0.0,0.0,0.0),
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Length of each edge").toLatin1().data()) = 2.0);

  generator.def("addTriangulatedPlaneFlat", &PrimitivesGeneratorPlugin::addTriangulatedPlaneFlat,
                                  QCoreApplication::translate("PythonDocPrimitivesGenerator","Generates a flat triangular planar mesh in the z plane (ObjectId is returned)").toLatin1().data(),
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Corner position").toLatin1().data()) = Vector(0.0,0.0,0.0),
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Size in X direction").toLatin1().data()) = 2.0,
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Size in Y direction").toLatin1().data()) = 2.0,
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Number of linesegments in x direction").toLatin1().data()) = 10,
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Number of linesegments in y direction").toLatin1().data()) = 10);

  generator.def("addTriangulatedPlaneBumpy", &PrimitivesGeneratorPlugin::addTriangulatedPlaneBumpy,
                                  QCoreApplication::translate("PythonDocPrimitivesGenerator","Generates a bumpy triangular planar mesh in the z plane (ObjectId is returned)").toLatin1().data(),
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Corner position").toLatin1().data()) = Vector(0.0,0.0,0.0),
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Size in X direction").toLatin1().data()) = 10.0,
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Size in Y direction").toLatin1().data()) = 10.0,
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Number of linesegments in x direction").toLatin1().data()) = 200,
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Number of linesegments in y direction").toLatin1().data()) = 200);


  generator.def("addTriangulatedPlanesNonManifold", &PrimitivesGeneratorPlugin::addTriangulatedPlanesNonManifold,
                                  QCoreApplication::translate("PythonDocPrimitivesGenerator","Generates two perpendicular planes in a non manifold config (ObjectId is returned)").toLatin1().data(),
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Corner position").toLatin1().data()) = Vector(0.0,0.0,0.0),
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Size in X direction").toLatin1().data()) = 2.0,
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Size in Y direction").toLatin1().data()) = 2.0,
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Size in Z direction").toLatin1().data()) = 1.0,
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Number of linesegments in x direction").toLatin1().data()) = 50,
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Number of linesegments in y direction").toLatin1().data()) = 50,
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Number of linesegments in y direction").toLatin1().data()) = 50);

  generator.def("addIcosahedron", &PrimitivesGeneratorPlugin::addIcosahedron,
                                  QCoreApplication::translate("PythonDocPrimitivesGenerator","Generates an Icosahedron (ObjectId is returned)").toLatin1().data(),
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Center position").toLatin1().data()) = Vector(0.0,0.0,0.0),
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Length of each edge").toLatin1().data()) = 2.0);

  generator.def("addOctahedron", &PrimitivesGeneratorPlugin::addOctahedron,
                                  QCoreApplication::translate("PythonDocPrimitivesGenerator","Generates an Octahedron (ObjectId is returned)").toLatin1().data(),
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Center position").toLatin1().data()) = Vector(0.0,0.0,0.0),
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Length of each edge").toLatin1().data()) = 2.0);

  generator.def("addDodecahedron", &PrimitivesGeneratorPlugin::addDodecahedron,
                                  QCoreApplication::translate("PythonDocPrimitivesGenerator","Generates a Dodecahedron (ObjectId is returned)").toLatin1().data(),
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Center position").toLatin1().data()) = Vector(0.0,0.0,0.0),
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Length of each edge").toLatin1().data()) = 2.0);

#ifdef ENABLE_BSPLINECURVE_SUPPORT
  generator.def("addRandomBSplineCurve", &PrimitivesGeneratorPlugin::addRandomBSplineCurve,
                                  QCoreApplication::translate("PythonDocPrimitivesGenerator","Generates a random B-spline curve (ObjectId is returned)").toLatin1().data(),
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Position").toLatin1().data()) = Vector(0.0,0.0,0.0),
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Number of control points").toLatin1().data()) = 5);
#endif


#ifdef ENABLE_BSPLINESURFACE_SUPPORT
  generator.def("addRandomBSplineSurface", &PrimitivesGeneratorPlugin::addRandomBSplineSurface,
                                  QCoreApplication::translate("PythonDocPrimitivesGenerator","Generates a random B-spline surface (ObjectId is returned)").toLatin1().data(),
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Position").toLatin1().data()) = Vector(0.0,0.0,0.0),
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Number of control points").toLatin1().data()) = 5);
#endif

#ifdef ENABLE_POLYHEDRALMESH_SUPPORT
  generator.def("addTetrahedralCube", &PrimitivesGeneratorPlugin::addTetrahedralCube,
                                  QCoreApplication::translate("PythonDocPrimitivesGenerator","Generates a tetrahedral mesh of a cube (ObjectId is returned)").toLatin1().data(),
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Center Position").toLatin1().data()) = Vector(0.0,0.0,0.0),
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Length of each edge").toLatin1().data()) = 2.0);

  generator.def("addTetrahedralCuboid", &PrimitivesGeneratorPlugin::addTetrahedralCuboid,
                                  QCoreApplication::translate("PythonDocPrimitivesGenerator","Generates a tetrahedral mesh of a cuboid (ObjectId is returned)").toLatin1().data(),
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Center Position").toLatin1().data()) = Vector(0.0,0.0,0.0),
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Length of each side").toLatin1().data()) = Vector(4.0,6.0,12.0),
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Number of units in x-axis").toLatin1().data()) = 5,
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Number of units in y-axis").toLatin1().data()) = 5,
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Number of units in z-axis").toLatin1().data()) = 10);

#endif

#ifdef ENABLE_HEXAHEDRALMESH_SUPPORT
  generator.def("addHexahedralCube", &PrimitivesGeneratorPlugin::addHexahedralCube,
                                  QCoreApplication::translate("PythonDocPrimitivesGenerator","Generates a hexahedral mesh of a cube (ObjectId is returned)").toLatin1().data(),
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Center Position").toLatin1().data()) = Vector(0.0,0.0,0.0),
                          py::arg(QCoreApplication::translate("PythonDocPrimitivesGenerator","Length of each edge").toLatin1().data()) = 2.0);

#endif

}
